'''
Que 4: WAP to print the following pattern
Take input from the user
1 3 5 7
2 4 6 8
9 11 13 15
10 12 14 16
'''

row=int(input("Enter the row :  "))
col=int(input("Enter the Col :  "))
sum=1

for i in range(row):
    for j in range(col):
        print(sum,end=" ")
        sum=sum+2
    print()
