'''
Que 2: WAP to print the following pattern
Take row input from the user

a
A B
a b c
A B C D
'''


row=int(input("Enter the row: "))
for i in range(row):
    for j in range(i+1):
        if(i%2==0):
            print(chr(96+j+1),end=" ")
        else:
            print(chr(64+j+1),end=" ")
    print()





