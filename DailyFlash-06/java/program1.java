/*
Que 1: WAP to print the following pattern
Take input from the user
A B C D
1 3 5 7
A B C D
9 11 13 15
A B C D
*/

import java.io.*;
import java.util.*;

class Demo {
	public static void main(String args[])throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the Row and Col : ");
		int row=Integer.parseInt(br.readLine());

		int num=1;

		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(i%2!=0){
					System.out.print(((char)(64+j))+" ");
				}else{
					System.out.print(num+" ");
					num=num+2;
				}
			}
			System.out.println();
		}
	}
}
