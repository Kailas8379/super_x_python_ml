'''
Que 1: WAP to print the following pattern
Take input from the user
A B C D
1 3 5 7
A B C D
9 11 13 15
A B C D
'''
x=int(input("Enter the row and col  :  "))
k=1
for i in range(x):
    for j in range(x):
        if(i%2==0):
            print(chr(64+j+1),end=" ")
        else:
            print(k,end=" ")
            k=k+2
    print()
