'''
Que 2: WAP to print the following pattern
Take row input from the user

A
B A
C B A
D C B A
'''

for i in range(4):
    k=i
    for j in range(i+1):
        print(chr(65+k),end=" ")
        k=k-1
    print()

