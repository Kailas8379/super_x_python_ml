/*
Que 1: WAP to print the following pattern
Take input from the user

A  B  C  D
#  #  #  #
A  B  C  D
#  #  #  #
A  B  C  D
*/
import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the row And Col ");
		int num=Integer.parseInt(br.readLine());

		for(int i=1; i<=num; i++){
			for(int j=1; j<=num; j++){

				if(i%2==0){
					System.out.print("# ");
				}else{
					System.out.print(((char)(64+j))+" ");
				}
			}
			System.out.println();
		}
	}
}
