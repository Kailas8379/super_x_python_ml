/*
Que 4 : WAP to print the composite numbers in the given range
input: start:1
end:100
*/

import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{
		
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the start : ");
		int s=Integer.parseInt(br.readLine());

		System.out.println("Enter the end : ");
		int end=Integer.parseInt(br.readLine());
			for(int i=s; i<=end; i++){
				for(int j=2; j<i-1; j++){
					if(i%j==0){
						System.out.print(i+ " ,");
						break;
					}
				}
			}
	}
}



