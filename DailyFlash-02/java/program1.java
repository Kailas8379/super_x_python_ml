/*
Que 1 : WAP to print the following pattern
  Take input from user
A B C D
B C D E
C D E F
D E F G
*/
import java.io.*;
import java.util.*;

class demo{
	public static void main(String args[])throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);
		
		System.out.println("Enter the Row and Col ");
		int col=Integer.parseInt(br.readLine());

		System.out.println("pattern is : ");
	int k=1;
	for(int i=1; i<=col; i++){
		for(int j=1; j<=col; j++){
			System.out.print(((char)(64+k))+"  ");
			k++;
		}
		System.out.println();
	}
}
}
