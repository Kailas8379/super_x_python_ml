/*
Que 2 : WAP to print the following pattern
Take row input from user
1
2 4
3 6 9
4 8 12 16
*/
import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the row and col : ");
		int col=Integer.parseInt(br.readLine());

		

		for(int i=1; i<=col; i++){
			int num=i;
			for(int j=1; j<=i;  j++){
				System.out.print(num*j+"  ");
				
			}
			System.out.println();
		}
	}
}

