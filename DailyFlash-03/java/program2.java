import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the Row And col : ");
		int num=Integer.parseInt(br.readLine());

		for(int i=1; i<=num; i++){
			int k=i;
			for(int j=1; j<=i; j++){
				System.out.print(k--+"  ");
			}
			System.out.println();
		}
	}
}

		
