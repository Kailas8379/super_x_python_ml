import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{
		
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the number ");
		int num=Integer.parseInt(br.readLine());
		int num1=num;
		int rev=0;

		while(num!=0){
			int rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		System.out.println(rev);
	}
}
