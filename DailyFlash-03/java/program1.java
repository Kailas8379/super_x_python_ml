

import java.io.*;
import java.util.*;

class Demo{
	public static void main(String args[])throws IOException{

		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		System.out.println("Enter the row And Col ");
		int row=Integer.parseInt(br.readLine());


		for(int i=1; i<=row; i++){
			int k=1;
			for(int j=row; j>=1; j--){
				if(i%2==0){
					System.out.print(((char)(64+j))+"  ");
				}else{
					System.out.print(((char)(64+k++))+"  ");
				
				}
			}
			System.out.println();
		}
	}
}


