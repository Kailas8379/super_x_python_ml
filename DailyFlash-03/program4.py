# Que 4 : WAP to print each reverse numbers in the given range
# Input: start:25435
# end: 25449
num=int(input("Enter the number : "))

num2=num
rev=0

while(num!=0):
    rem=num%10
    rev=rev*10+rem
    num=num//10

print(rev)
