'''
Que 1: WAP to print the following pattern
Take input from the user
1 2 3 4
a b c d
5 6 7 8
e f g h
9 10 11 12
'''

row=int(input("Enter the Row and Col :  "))
num=1
al=97
for i in range(1,row+1):
    for j in range(1,row+1):
        if(i%2!=0):
            print(num,end=" ")
            num=num+1;
        else:
            print(chr (al),end=" ")
            al=al+1
    print()
