'''
Que 2: WAP to print the following pattern
Take row input from the user
10
9 8
7 6 5
4 3 2 1
'''
row=int(input("Enter the Row And Col :  "))
num=(row*(row+1))//2
for i in range(row):
    for j in range(i+1):
        print(num,end=" ")
        num=num-1
    print()
